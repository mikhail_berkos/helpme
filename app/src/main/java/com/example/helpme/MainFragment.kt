package com.example.helpme

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.SupportActionModeWrapper
import androidx.core.net.toUri
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.helpme.DataClasses.Field
import com.example.helpme.DataClasses.Form
import com.example.helpme.DataClasses.Post
import com.example.helpme.Retrofit.RetrofitService.retrofitData
import com.example.helpme.databinding.FragmentMainBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainFragment : Fragment() {
    lateinit var binding: FragmentMainBinding
    private lateinit var adapter: RecyclerViewAdapter
    lateinit var list : MutableList<Post>

    val listData: ArrayList<Field> = ArrayList()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            FragmentMainBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initForm()

    }



    private fun initForm() {
        retrofitData.enqueue(object : Callback<Form> {
            override fun onResponse(
                call: Call<Form>,
                response: Response<Form>
            ) {
                val responseBody = response.body()!!
                (activity as AppCompatActivity).supportActionBar?.title = responseBody.title

                val recyclerView = view?.findViewById<RecyclerView>(R.id.recyclerView)
                recyclerView?.layoutManager = LinearLayoutManager(activity)


                listData.addAll(responseBody.fields)
                listData.addAll(responseBody.fields)






                adapter = RecyclerViewAdapter(listData)
                recyclerView?.adapter = adapter

                Glide.with(binding.imageView3).load(responseBody.image.toUri())
                    .into(binding.imageView3)

                response.body()!!.fields.forEach {

                    Log.e("RE", it.toString())

                }

            }

            override fun onFailure(call: Call<Form>, t: Throwable) {
                Log.e("TAG", "WTF $t")

            }
        })
        binding.button.setOnClickListener {
            if (!list.isEmpty()) {
                list.forEach { Log.e("AAA", it.fieldName + it.fieldValue) }
            } else {


                Log.e("AAA", "Хуле не работает")
            }
        }



    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         *
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() = MainFragment()


    }

}