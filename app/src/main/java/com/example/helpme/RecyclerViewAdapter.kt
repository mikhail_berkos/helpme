package com.example.helpme

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.helpme.DataClasses.Field
import com.example.helpme.DataClasses.Post


class RecyclerViewAdapter(val listData : List<Field>) : RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewAdapter.MyViewHolder{
       val view =  LayoutInflater.from(parent.context).inflate(R.layout.recycler_row, parent, false)
        Log.e("AAA", "In RV OnCreateViewHolder_______________________________________________________")

        return MyViewHolder(view)
    }

    override fun onViewRecycled(holder: MyViewHolder) {
        super.onViewRecycled(holder)
        MainFragment().list.add(Post(holder.titleTextView.text.toString(), holder.titleTextView2.text.toString()))

        Log.e("AAA", "In RV ДОБАВЛЕНИЕ В СПИСОК_______________________________________________________")




    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


       holder.titleTextView.text = listData[position].title
        if (listData[position].name == "text")
        holder.titleTextView2.hint = "Поле для ввода текста"
        if (listData[position].name == "numeric")
            holder.titleTextView2.hint = "Поле для ввода чисел"
        if (listData[position].name == "list")
            holder.titleTextView2.hint = "Тут спиннер"




        Log.e("AAA", "In RV OnBindViewHolder_______________________________________________________")

    }

    override fun getItemCount(): Int {
        return listData.size
    }
    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var titleTextView : TextView = view.findViewById(R.id.titleTextview)
        var titleTextView2 : EditText = view.findViewById(R.id.titleTextview2)



    }



}